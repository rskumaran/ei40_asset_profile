package com.entappia.ei4oassetprofile.models;
 
 public class ConfigData {
	String name;
	String type; 
	String defaultval; 
	long modifiedTime; 

 	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	 
	public String getName() {
		return name;
	}

 	public void setName(String name) {
		this.name = name;
	}

 	 
 	public long getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(long modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

 	public String getDefaultval() {
		return defaultval;
	}

	public void setDefaultval(String defaultval) {
		this.defaultval = defaultval;
	}
 
}
