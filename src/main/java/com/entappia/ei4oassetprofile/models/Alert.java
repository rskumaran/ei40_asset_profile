package com.entappia.ei4oassetprofile.models;

public class Alert {
	int sms;
	int mail;
 
	public int getSms() {
		return sms;
	}

	public void setSms(int sms) {
		this.sms = sms;
	}
 
	public int getMail() {
		return mail;
	}

	public void setMail(int mail) {
		this.mail = mail;
	}

}
