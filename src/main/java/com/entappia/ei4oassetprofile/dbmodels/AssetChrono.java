package com.entappia.ei4oassetprofile.dbmodels;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4oassetprofile.constants.AppConstants;
import com.entappia.ei4oassetprofile.constants.AppConstants.AssignmentStatus;
import com.entappia.ei4oassetprofile.converters.AssetAssignmentDetailsConverter;
import com.entappia.ei4oassetprofile.models.AssetAssignmentDetails;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "AssetChrono")
public class AssetChrono {
	@Id
	@Column(length = 12)
	String assetId;
	@Column(length = 20)
	String assetType;

	@Column(length = 20)
	String subType;
	
	@Column(length = 20)
	String type;
	
	@Column(length = 20)
	String rangeValue;
	
	@Column(length = 30)
	String manufacturer;

	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "YYYY-MMM-dd", timezone= AppConstants.timeZone)
	Date validityDate;
 
	@Enumerated
	@Column(name = "assignmentStatus", length = 8)
	AssignmentStatus assignmentStatus;

	@Convert(converter = AssetAssignmentDetailsConverter.class)
	@Column(name = "assetAssignmentDetails", columnDefinition = "json")
	HashMap<String, AssetAssignmentDetails> assetAssignmentDetails;
	

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date modifiedDate;

	boolean active;
	
	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRangeValue() {
		return rangeValue;
	}

	public void setRangeValue(String rangeValue) {
		this.rangeValue = rangeValue;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	} 

	public HashMap<String, AssetAssignmentDetails> getAssetAssignmentDetails() {
		return assetAssignmentDetails;
	}

	public void setAssetAssignmentDetails(HashMap<String, AssetAssignmentDetails> assetAssignmentDetails) {
		this.assetAssignmentDetails = assetAssignmentDetails;
	}

	public Date getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(Date validityDate) {
		this.validityDate = validityDate;
	}

	public AssignmentStatus getAssignmentStatus() {
		return assignmentStatus;
	}

	public void setAssignmentStatus(AssignmentStatus assignmentStatus) {
		this.assignmentStatus = assignmentStatus;
	}
  
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	} 

}
