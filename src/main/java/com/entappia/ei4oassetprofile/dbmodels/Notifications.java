package com.entappia.ei4oassetprofile.dbmodels;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.json.JSONObject;

import com.entappia.ei4oassetprofile.constants.AppConstants;
import com.entappia.ei4oassetprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4oassetprofile.converters.NotificationMessagesConverter;
import com.entappia.ei4oassetprofile.models.NotificationDetailMessages;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Notifications")
public class Notifications {
	
	@Id
	@Column(nullable = false, columnDefinition = "SMALLINT")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private long  id;
	

	@Enumerated
	@Column(name = "type", length = 2)
	NotificationType type;
	 
	@Column(length = 20)
	String source;

	String message;
	
	@Convert(converter = NotificationMessagesConverter.class)
	@Column(name = "messageObj", columnDefinition = "json")
	NotificationDetailMessages messageObj;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss.SSS z", timezone= AppConstants.timeZone)
	private Date notifyTime;
  
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	} 

	public NotificationDetailMessages getMessageObj() {
		return messageObj;
	}

	public void setMessageObj(NotificationDetailMessages messageObj) {
		this.messageObj = messageObj;
	}

	public Date getNotifyTime() {
		return notifyTime;
	}

	public void setNotifyTime(Date notifyTime) {
		this.notifyTime = notifyTime;
	}
	
	

}
