package com.entappia.ei4oassetprofile.dbmodels;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.entappia.ei4oassetprofile.constants.AppConstants;
import com.entappia.ei4oassetprofile.converters.LogsConverter;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "AssetLogs")
public class AssetLogs implements Serializable {

	@Id
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm:ss.SSS z")
	private Date date;

	@Column(length = 500)
	private String result;
	private String minor;
	private String major;
	@Column(length = 10)
	private String type;
	
	@Convert(converter = LogsConverter.class)
	@Column(name = "parameter", columnDefinition = "json")
	private Map<String, String> parameter;
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	} 
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMinor() {
		return minor;
	}

	public void setMinor(String minor) {
		this.minor = minor;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public Map<String, String> getParameter() {
		return parameter;
	}

	public void setParameter(Map<String, String> parameter) {
		this.parameter = parameter;
	}

	 
}
