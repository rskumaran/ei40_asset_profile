package com.entappia.ei4oassetprofile.dbmodels;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ZoneClassification")
public class ZoneClassification {

	@Id
	@Column(nullable = false, columnDefinition = "SMALLINT")
	int id;

	@Column(length = 15)
	String className;
	@Column(length = 17)
	String Color;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getColor() {
		return Color;
	}

	public void setColor(String color) {
		Color = color;
	}

}
