package com.entappia.ei4oassetprofile.dbmodels;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "CalibrationRecord")
public class CalibrationRecord {

	@Id
	@Column(length = 12)
	String calibrationId;

	@Column(length = 20)
	String asset;

	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "YYYY-MMM-DD")
	Date date;

	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "YYYY-MMM-DD")
	Date calibrationExpiryDate;

	@Lob
	private Blob calibrationData;

	public String getCalibrationId() {
		return calibrationId;
	}

	public void setCalibrationId(String calibrationId) {
		this.calibrationId = calibrationId;
	}

	public String getAsset() {
		return asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getCalibrationExpiryDate() {
		return calibrationExpiryDate;
	}

	public void setCalibrationExpiryDate(Date calibrationExpiryDate) {
		this.calibrationExpiryDate = calibrationExpiryDate;
	}

	public Blob getCalibrationData() {
		return calibrationData;
	}

	public void setCalibrationData(Blob calibrationData) {
		this.calibrationData = calibrationData;
	}
	
	
	
}
