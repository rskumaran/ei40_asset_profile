package com.entappia.ei4oassetprofile.dbmodels;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.entappia.ei4oassetprofile.constants.AppConstants;
import com.entappia.ei4oassetprofile.converters.HashMapListConverter;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "CampusDetails")
public class CampusDetails {

	@Id
	@Column(nullable = false, columnDefinition = "SMALLINT")
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private long campusId;

	@Column(precision = 15, scale = 2)
	float width;

	@Column(precision = 15, scale = 2)
	float height;
	 
	double metersPerPixelX;
	double metersPerPixelY;
	double originX;
	double originY;
	

	@Column(length = 20)
	String name;

	@Column(length = 10)
	String colorCode;
	
	@Convert(converter = HashMapListConverter.class)
	@Column(name = "gridZoneDetails", columnDefinition = "json") 
	HashMap<String, List<String>> gridZoneDetails;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm", timezone = AppConstants.timeZone)
	Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern = "yyyy-MMM-dd HH:mm", timezone = AppConstants.timeZone)
	Date modifiedDate;
	
	boolean isActive;

	public long getCampusId() {
		return campusId;
	}

	public void setCampusId(long campusId) {
		this.campusId = campusId;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	
	public double getMetersPerPixelX() {
		return metersPerPixelX;
	}

	public void setMetersPerPixelX(double metersPerPixelX) {
		this.metersPerPixelX = metersPerPixelX;
	}

	public double getMetersPerPixelY() {
		return metersPerPixelY;
	}

	public void setMetersPerPixelY(double metersPerPixelY) {
		this.metersPerPixelY = metersPerPixelY;
	}

	public double getOriginX() {
		return originX;
	}

	public void setOriginX(double originX) {
		this.originX = originX;
	}

	public double getOriginY() {
		return originY;
	}

	public void setOriginY(double originY) {
		this.originY = originY;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	

	public HashMap<String, List<String>> getGridZoneDetails() {
		return gridZoneDetails;
	}

	public void setGridZoneDetails(HashMap<String, List<String>> gridZoneDetails) {
		this.gridZoneDetails = gridZoneDetails;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	
}
