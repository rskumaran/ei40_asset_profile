package com.entappia.ei4oassetprofile.dbmodels;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.entappia.ei4oassetprofile.constants.AppConstants.AssignmentType;
import com.entappia.ei4oassetprofile.constants.AppConstants;
import com.entappia.ei4oassetprofile.dbmodels.idclass.AssetAllocationIdClass;
import com.fasterxml.jackson.annotation.JsonFormat;
 

@Entity
@Table(name = "AssetNonChronoAllocation")
@IdClass(AssetAllocationIdClass.class)
public class AssetNonChronoAllocation implements Serializable{
	
	@Id
	@Column(length = 12)
	String assetId;
	  
	@Id 
	@Column(length = 30)
	String macId;
 
	@Enumerated
	@Column(name = "assignmentType", length = 1)
	AssignmentType assignmentType;
   
	@Column(length = 20)
	String status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date allocationStartTime;
	
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MMM-dd HH:mm:ss", timezone= AppConstants.timeZone)
	Date allocationEndTime;

	public String getAssetId() {
		return assetId;
	}

	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}

	public String getMacId() {
		return macId;
	}

	public void setMacId(String macId) {
		this.macId = macId;
	}
  
	public AssignmentType getAssignmentType() {
		return assignmentType;
	}

	public void setAssignmentType(AssignmentType assignmentType) {
		this.assignmentType = assignmentType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getAllocationStartTime() {
		return allocationStartTime;
	}

	public void setAllocationStartTime(Date allocationStartTime) {
		this.allocationStartTime = allocationStartTime;
	}

	public Date getAllocationEndTime() {
		return allocationEndTime;
	}

	public void setAllocationEndTime(Date allocationEndTime) {
		this.allocationEndTime = allocationEndTime;
	}

	 

}
