package com.entappia.ei4oassetprofile.udp.client;

public interface UdpClient {
	public void sendMessage(String message);
}
