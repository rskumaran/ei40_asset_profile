package com.entappia.ei4oassetprofile.bgtasks;

import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;

import com.entappia.ei4oassetprofile.constants.AppConstants;
import com.entappia.ei4oassetprofile.utils.Preference;
import com.entappia.ei4oassetprofile.utils.Utils;


 
 
@Component
public class TagSchedulerTask {
	private static ThreadPoolTaskScheduler tagApiTaskScheduler;
	private static boolean isRunning;
	public String TAG_NAME = "TagSchedulerTask";

	Preference preference = new Preference();
	
public void createSchedule() {

		if (tagApiTaskScheduler == null) {
			tagApiTaskScheduler = new ThreadPoolTaskScheduler();
			tagApiTaskScheduler.setPoolSize(5);
			tagApiTaskScheduler.initialize();
		}

		startSchedule();
	}

	public void startSchedule() {

		String cronTime = "0 0/1 * * * *";
		
		Utils.printInConsole("HelloApiTask cronTime: " + cronTime);

		if (!isRunning) {
			if (tagApiTaskScheduler != null)
				tagApiTaskScheduler.schedule(new RunnableTask(), new CronTrigger(cronTime));

			isRunning = true;
		}
	}

	public void stopSchedule() {
		if (tagApiTaskScheduler != null)
		{
			tagApiTaskScheduler.getScheduledThreadPoolExecutor().shutdown();
			tagApiTaskScheduler.shutdown();
		}

		tagApiTaskScheduler = null;
		isRunning = false;
	}

	public void changeScheduleTime() {
		stopSchedule();
		createSchedule();
		startSchedule();
	}
	
	class RunnableTask implements Runnable {

		public RunnableTask() {
		}

		@SuppressWarnings("unused")
		@Override
		public void run() {

			//tagScheduleTask.getTagPositions();
		}
	};
	
}
