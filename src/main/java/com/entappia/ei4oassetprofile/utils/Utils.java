package com.entappia.ei4oassetprofile.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.entappia.ei4oassetprofile.constants.AppConstants;

public class Utils {


	public static boolean isEmptyString(String value) {
		return value == null || value.trim().isEmpty() || value.equals("null");
	}

	public static String encryption(String value) {
		return AES.encrypt(value, AppConstants.seckretKey);
	}

	public static String decryption(String value) {
		return AES.decrypt(value, AppConstants.seckretKey);
	}

	public static boolean validateDate(String strDate) {

		String strDateRegEx = "\\d{4}-(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)-(0[1-9]|[12][0-9]|[3][01])";

		if (strDate.matches(strDateRegEx)) {
			printInConsole(strDate + " is valid");
			return true;
		} else {
			printInConsole(strDate + " is not valid");
			return false;
		}

	}

	public static String getCookie(HttpServletRequest request, String name) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(name)) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}

	public static HashMap<String, List<String>> getGridHashmap(int width, int height) {
		HashMap<String, List<String>> campusGridZoneMap = new HashMap<>();
		HashMap<String, List<HashMap<String, String>>> campusGridMap = new HashMap<>();

		for (int i = 0; i < height; i++) {

			for (int j = 0; j < width; j++) {


				List<HashMap<String, String>> mapList = new ArrayList<>();
				HashMap<String, String> map = new HashMap<>();
				map.put("x", "" + i);
				map.put("y", "" + j);
				mapList.add(map);

				HashMap<String, String> map1 = new HashMap<>();
				map1.put("x", "" + i);
				map1.put("y", "" + (j + 1));
				mapList.add(map1);

				HashMap<String, String> map2 = new HashMap<>();
				map2.put("x", "" + (i + 1));
				map2.put("y", "" + (j + 1));
				mapList.add(map2);

				HashMap<String, String> map3 = new HashMap<>();
				map3.put("x", "" + (i + 1));
				map3.put("y", "" + j);
				mapList.add(map3);

				campusGridMap.put(i + "_" + j, mapList);
				campusGridZoneMap.put(i + "_" + j, new ArrayList());
			}

		}

		return campusGridZoneMap;
	}


	public static String getDate(Date date) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(date);

	}

	public static String getFormatedDate(Date date) {
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
		return dateTimeFormat.format(date);

	}

	public static String getFormatedDate1(Date date) {
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
		return dateTimeFormat.format(date);

	}

	public static String getFormatedDate2(Date date) {
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss.SSS");
		return dateTimeFormat.format(date);

	}



	public static Date getDate(String date) {
		try {
			printInConsole("Date:" + date);
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			return dateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	public static String getDateTime1(Date date) {
		SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return DATE_TIME_FORMAT.format(date);
	}

	public static Date getDateTime(String date) {
		try {
			SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			return DATE_TIME_FORMAT.parse(date);
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}

	}

	public static String getTime(Date date) {
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
		return timeFormat.format(date);
	}

	public static int getDayMintus(Date date) {
		String sTime = Utils.getTime(date);
		String hours = sTime.split(":")[0];
		String mins = sTime.split(":")[1];

		int min = Integer.valueOf(mins);

		int dayMintus = 0;
		if (Integer.valueOf(hours) == 0) {
			dayMintus = (min);
		} else {
			dayMintus = ((Integer.valueOf(hours) * 60) + min);
		}

		return dayMintus;
	}

	public static Date stringToDate(String date) {

		try {
			return new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	
	public static void printInConsole(Integer message) {
			printInConsole(message.toString());
	}
	public static void printInConsole(String message) {
		
		printInConsole(message, false);
	}
	

	public static void printInConsole(String message, boolean forcePrint) {
		
		if (AppConstants.print_log || forcePrint) {
			System.out.println(message);
		}
		
	}

}
