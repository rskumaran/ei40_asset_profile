package com.entappia.ei4oassetprofile.utils;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import org.springframework.stereotype.Component;

@Component
public class Preference {

	private static Preferences prefs;
 
    
	public Preference() {
		// This will define a node in which the preferences can be stored

		if (prefs == null)
			prefs = Preferences.userRoot().node(this.getClass().getName());
	}

	// Delete the preference
	public void clear() {
		try {
			prefs.clear();
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Delete the preference settings
	public void removePrefValue(String prefName) {
		prefs.remove(prefName);
	}

	// set the boolean values
	public void setBooleanValue(String prefName, boolean value) {
		// TODO Auto-generated method stub
		prefs.putBoolean(prefName, value);
	}

	// set the int values
	public void setIntValue(String prefName, int value) {
		// TODO Auto-generated method stub
		prefs.putInt(prefName, value);
	}

	// set the string values
	public void setStringValue(String prefName, String value) {
		// TODO Auto-generated method stub
		prefs.put(prefName, value);
	}

	// get the boolean values
	public boolean getBooleanValue(String prefName, boolean defultvalue) {
		// TODO Auto-generated method stub
		return prefs.getBoolean(prefName, defultvalue);
	}

	// get the int values
	public int getIntValue(String prefName, int defultvalue) {
		// TODO Auto-generated method stub
		return prefs.getInt(prefName, defultvalue);
	}

	// get the string values
	public String getStringValue(String prefName) {
		// TODO Auto-generated method stub
		return prefs.get(prefName, "");
	}

	// get the string values
	public String getStringValue(String prefName, String defultvalue) {
		// TODO Auto-generated method stub
		return prefs.get(prefName, defultvalue);
	}

	// get the long values
	public void setLongValue(String prefName, long defultvalue) {
		// TODO Auto-generated method stub
		prefs.putLong(prefName, defultvalue);
	}

	// get the long values
	public long getLongValue(String prefName) {
		// TODO Auto-generated method stub
		return prefs.getLong(prefName, 0);
	}
}
