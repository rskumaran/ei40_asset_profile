package com.entappia.ei4oassetprofile.utils;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entappia.ei4oassetprofile.constants.AppConstants.LogEventType;
import com.entappia.ei4oassetprofile.dbmodels.AssetLogs;
import com.entappia.ei4oassetprofile.repository.AssetLogsRepository;




@Service
public class LogEvents {

	@Autowired
	AssetLogsRepository assetLogsRepository;

	public synchronized void addLogs(LogEventType type, String major, String minor, String message) {
		if (assetLogsRepository != null) {
			AssetLogs logs = new AssetLogs();
			logs.setDate(new Date());

			if (LogEventType.ERROR == type)
				logs.setType("Error");
			else
				logs.setType("Event");
			logs.setMajor(major);
			logs.setMinor(minor);
			logs.setResult(message);
			assetLogsRepository.save(logs);
		}
	}

}
