package com.entappia.ei4oassetprofile.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.entappia.ei4oassetprofile.constants.AppConstants; 

public abstract class BaseController extends HttpServlet {
 

	public HttpSession handleSession(HttpSession session, String orgName, String userType, String mailId,
			String timeZone, HashMap<String, HashMap<String, Integer>> capabilityDetailes, String serviceProvider,
			String userName) {
		if (session.getAttribute("mailId") != mailId) {
			session.setAttribute("orgName", orgName);
			session.setAttribute("userType", userType);
			session.setAttribute("mailId", mailId);
			session.setAttribute("timeZone", timeZone);
			session.setAttribute("capabilityDetailes", capabilityDetailes);
			session.setAttribute("serviceProvider", serviceProvider);
			session.setAttribute("userName", userName);
			session.setAttribute("isActive", true);

			if (userType.equals("mobile"))
				session.setMaxInactiveInterval(4380 * 3600); // session timeout in seconds
			else
				session.setMaxInactiveInterval(1800); // session timeout in seconds
 
		}
		return session;
	}


	public ResponseEntity<?> checkSessionKey(HttpSession httpSession, String session_id) {

		Map<String, Object> returnmap = new HashMap<>();
		String sessionId = httpSession.getId();

		if (Utils.isEmptyString(session_id)) {
			returnmap.put("status", "error");
			returnmap.put("message", "403 Forbidden or No Permission to Access");
			return new ResponseEntity<>(returnmap, HttpStatus.FORBIDDEN);
		}
		if (!sessionId.equals(session_id) || httpSession.getAttribute("isActive") == null) {

			returnmap.put("status", "error");
			returnmap.put("message", AppConstants.sessiontimeout);
			return new ResponseEntity<>(returnmap, HttpStatus.FORBIDDEN);
		}
 
		 
		return null;

	}

	public ResponseEntity<?> changeSessionTime(HttpSession httpSession, String session_id, boolean isStart) {
		String sessionId = httpSession.getId();

		if (!Utils.isEmptyString(session_id) && sessionId.equals(session_id)) {
			if (isStart)
				httpSession.setMaxInactiveInterval(4380 * 3600);
			else
				httpSession.setMaxInactiveInterval(1800);
		}
		return null;
	}
}
