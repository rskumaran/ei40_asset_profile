package com.entappia.ei4oassetprofile.converters;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Converter
public class CoordinateConverter  implements AttributeConverter<List<HashMap<String, String>>, String> {

	Gson gson = new Gson();
	Type mapType = new TypeToken<List<HashMap<String, String>>>() {
	}.getType();

	@Override
	public String convertToDatabaseColumn(List<HashMap<String, String>> data) {
		if (null == data) {
			// You may return null if you prefer that style
			return "[]";
		}

		try {
			return gson.toJson(data, mapType);

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting map to JSON", e);
		}
	}

	@Override
	public List<HashMap<String, String>> convertToEntityAttribute(String s) {
		if (null == s || s.equals("[]")) {
			// You may return null if you prefer that style
			return new ArrayList();
		}

		try {

			List<HashMap<String, String>> mapList = gson.fromJson(s, mapType);

			return mapList;

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting JSON to map", e);
		}
	}

}
