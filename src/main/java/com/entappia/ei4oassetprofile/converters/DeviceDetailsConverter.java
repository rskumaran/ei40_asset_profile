package com.entappia.ei4oassetprofile.converters;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.entappia.ei4oassetprofile.models.DeviceDetails;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Converter
public class DeviceDetailsConverter implements AttributeConverter<List<DeviceDetails>, String> {

	Gson gson = new Gson();
	Type mapType = new TypeToken<List<DeviceDetails>>() {
	}.getType();

	@Override
	public String convertToDatabaseColumn(List<DeviceDetails> data) {
		if (null == data) {
			// You may return null if you prefer that style
			return "{}";
		}

		try {
			return gson.toJson(data, mapType);

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting map to JSON", e);
		}
	}

	@Override
	public List<DeviceDetails> convertToEntityAttribute(String s) {
		if (null == s || s.equals("{}")) {
			// You may return null if you prefer that style
			return new ArrayList<DeviceDetails>();
		}

		try {

			List<DeviceDetails> nameEmployeeMap = gson.fromJson(s, mapType);

			return nameEmployeeMap;

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting JSON to map", e);
		}
	}

}
