package com.entappia.ei4oassetprofile.converters;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.entappia.ei4oassetprofile.models.Alert; 
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Converter
public class OrgAlertConverter implements AttributeConverter<Map<String, Alert>, String> {

	Gson gson = new Gson();
	Type mapType = new TypeToken<Map<String, Alert>>() {
	}.getType();

	@Override
	public String convertToDatabaseColumn(Map<String, Alert> data) {
		if (null == data) {
			// You may return null if you prefer that style
			return "{}";
		}

		try {
			return gson.toJson(data, mapType);

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting map to JSON", e);
		}
	}

	@Override
	public Map<String, Alert> convertToEntityAttribute(String s) {
		if (null == s || s.equals("{}")) {
			// You may return null if you prefer that style
			return new HashMap<>();
		}

		try {

			Map<String, Alert> nameEmployeeMap = gson.fromJson(s, mapType);

			return nameEmployeeMap;

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting JSON to map", e);
		}
	}

}
