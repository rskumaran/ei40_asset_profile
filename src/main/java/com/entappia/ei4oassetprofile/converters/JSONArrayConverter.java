package com.entappia.ei4oassetprofile.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.json.JSONArray;
import org.json.JSONObject;

@Converter
public class JSONArrayConverter implements AttributeConverter<JSONArray, String> {
 

	@Override
	public String convertToDatabaseColumn(JSONArray data) {
		if (null == data) {
			// You may return null if you prefer that style
			return "[]";
		}

		try {
			return data.toString();

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting String to JSON", e);
		}
	}

	@Override
	public JSONArray convertToEntityAttribute(String s) {
		if (null == s || s.equals("[]")) {
			// You may return null if you prefer that style
			return new JSONArray();
		}

		try { 
			
			return new JSONArray(s);

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting JSON to String", e);
		}
	}
}
