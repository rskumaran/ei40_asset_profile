package com.entappia.ei4oassetprofile.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.json.JSONObject;

@Converter
public class JSONObjectConverter implements AttributeConverter<JSONObject, String> {
 

	@Override
	public String convertToDatabaseColumn(JSONObject data) {
		if (null == data) {
			// You may return null if you prefer that style
			return "{}";
		}

		try {
			return data.toString();

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting map to JSON", e);
		}
	}

	@Override
	public JSONObject convertToEntityAttribute(String s) {
		if (null == s || s.equals("{}")) {
			// You may return null if you prefer that style
			return new JSONObject();
		}

		try { 
			
			return new JSONObject(s);

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting JSON to map", e);
		}
	}
}
