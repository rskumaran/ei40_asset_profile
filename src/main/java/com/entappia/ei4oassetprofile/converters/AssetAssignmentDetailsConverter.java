package com.entappia.ei4oassetprofile.converters;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.persistence.AttributeConverter;

import com.entappia.ei4oassetprofile.models.AssetAssignmentDetails;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class AssetAssignmentDetailsConverter  implements AttributeConverter<HashMap<String, AssetAssignmentDetails>, String> {

	Gson gson = new Gson();
	Type mapType = new TypeToken<HashMap<String, AssetAssignmentDetails>>() {
	}.getType();

	@Override
	public String convertToDatabaseColumn(HashMap<String, AssetAssignmentDetails> data) {
		if (null == data) {
			// You may return null if you prefer that style
			return "{}";
		}

		try {
			return gson.toJson(data, mapType);

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting map to JSON", e);
		}
	}

	@Override
	public HashMap<String, AssetAssignmentDetails> convertToEntityAttribute(String s) {
		if (null == s || s.equals("{}")) {
			// You may return null if you prefer that style
			return new HashMap();
		}

		try {

			HashMap<String, AssetAssignmentDetails> mapList = gson.fromJson(s, mapType);

			return mapList;

		} catch (Exception e) {
			throw new IllegalArgumentException("Error converting JSON to map", e);
		}
	}

}
