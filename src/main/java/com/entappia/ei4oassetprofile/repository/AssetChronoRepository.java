package com.entappia.ei4oassetprofile.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4oassetprofile.dbmodels.AssetChrono;



@Repository
public interface AssetChronoRepository extends CrudRepository<AssetChrono, Integer> {
	
	AssetChrono findByAssetId(String assetId);
	/*select * from asset_chrono where active = 1 and asset_id in 
			 (select asset_id from asset_chrono_allocation where status like 'valid' and (assignment_type = 0 or assignment_type = 1));*/
	@Query(value = "select * from asset_chrono where active = 1 and asset_id in \r\n"
			+ "			 (select asset_id from asset_chrono_allocation where status like 'valid' and (assignment_type = 0 or assignment_type = 1));", nativeQuery = true)
	ArrayList<AssetChrono> getAllActiveValidAssets();
}
