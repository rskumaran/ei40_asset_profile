package com.entappia.ei4oassetprofile.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4oassetprofile.dbmodels.AssetNonChrono;

@Repository
public interface AssetNonChronoRepository extends CrudRepository<AssetNonChrono, Integer> {

AssetNonChrono findByAssetId(String assetId);
	
	List<AssetNonChrono> findAll();
	
	@Query(value = "select * from asset_non_chrono where active = 1 and asset_id in \r\n"
			+ "			 (select asset_id from asset_non_chrono_allocation where status like 'valid' and (assignment_type = 0 or assignment_type = 1));", nativeQuery = true)
	ArrayList<AssetNonChrono> getAllActiveValidAssets();
}
