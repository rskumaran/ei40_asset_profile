package com.entappia.ei4oassetprofile.repository;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.entappia.ei4oassetprofile.utils.Utils;

@Repository
public class TableUtils {


	@Autowired
	private EntityManagerFactory entityManagerFactory;


	public static final String createLocationTable = "CREATE TABLE IF NOT EXISTS `xxxxxxxxxx` (  `id` varchar(12) NOT NULL,  "
			+ "`time` int, `grid` smallint, zone varchar(12), zone_name varchar(30), `grid_pos` varchar(10), `duration` int,  PRIMARY KEY (`id`, `time`)) "
			+ "ENGINE=InnoDB  DEFAULT CHARSET=latin1;";


	public boolean createLocationTable(String tableName) {

		Session session = null;
		try {


			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			String sqlQueryString = "";
			sqlQueryString = createLocationTable;

			String sqlString = sqlQueryString.replace("xxxxxxxxxx",tableName);

			Utils.printInConsole(sqlString);
			Transaction transaction = session.beginTransaction();

			Query updateQuery = session.createNativeQuery(sqlString);

			updateQuery.executeUpdate();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return false;
		}finally {
			if (session!= null)
				session.close();
		} 

		return true;
	}
	public boolean insertIntoLocationTable(String tableName, String ID, int dayTimeInMinutes, int gridNumber, 
			int zoneID, String zoneName, String gridPos, int duration) {

		Session session = null;
		try {


			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();
			final String insert_location_query = "Insert IGNORE into `" + tableName + "` values ('" + ID + "', " 
					+ dayTimeInMinutes + ", " + gridNumber + ", " + zoneID + ", '" + zoneName + "', '"+ gridPos + "', " + duration + " )";

			Utils.printInConsole("Insert Query: " + insert_location_query );

			Transaction transaction = session.beginTransaction();

			Query updateQuery = session.createNativeQuery(insert_location_query);

			updateQuery.executeUpdate();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return false;
		}finally {
			if (session!= null)
				session.close();
		}

		return true;
	}
	public List<?> SelectTableNamesLike(String tableName, String lastDateToKeepLocationTable)
	{
		List<?> TableNames = null;
		String sqlQueryString = "SELECT table_name FROM INFORMATION_SCHEMA.TABLES where table_schema = 'db_ei40' and table_name like '" + tableName + "%'"
				+ " and DATE(create_time) < '" + lastDateToKeepLocationTable + "'";
		Session session = null;
		try {


			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query fetchQuery = session.createNativeQuery(sqlQueryString);

			TableNames = fetchQuery.getResultList();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {
			return TableNames;
		}finally {
			if (session!= null)
				session.close();
		} 

		return TableNames;

	}
	public int dropLocationTable(String tableName)
	{
		String sqlQueryString = "DROP Table `" + tableName + "`";
		int queryResult = -1;
		Session session = null;
		try {


			session = entityManagerFactory.unwrap(SessionFactory.class).openSession();

			Transaction transaction = session.beginTransaction();

			Query fetchQuery = session.createNativeQuery(sqlQueryString);

			queryResult = fetchQuery.executeUpdate();
			transaction.commit();

			if (session!= null)
				session.close();

		}catch (Exception e) {

		}finally {
			if (session!= null)
				session.close();
		} 
		return queryResult;
	}
	


}

