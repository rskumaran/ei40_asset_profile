package com.entappia.ei4oassetprofile.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4oassetprofile.dbmodels.Tags;

@Repository
public interface TagsRepository extends CrudRepository<Tags, Integer> {

	Tags findByTagId(String tagId);
	 
	Tags findByMacId(String macId);
	
	@Query(value = "select * from tags where mac=:mac and tag_id!=:tagid", nativeQuery = true)
	Tags checkMacId(@Param("mac") String mac,
			@Param("tagid") String tagid);
	
	List<Tags> findAll();
	
	@Query(value = "select * from tags where type like 'Asset Chrono' or type like 'Asset NonChrono'", nativeQuery = true)
	Iterable<Tags> selectAssetTags();
	
	@Query(value = "select * from tags where status = 'Assigned' and (type like 'Asset Chrono' or type like 'Asset NonChrono')", nativeQuery = true)
	Iterable<Tags> selectAssignedAssetTags();
	
	@Query(value = "select * from tags where status = 'Assigned' and type like 'Asset Chrono'", nativeQuery = true)
	Iterable<Tags> selectAssignedAssetChronoTags();
	
	@Query(value = "select * from tags where status = 'Assigned' and type like 'Asset NonChrono'", nativeQuery = true)
	Iterable<Tags> selectAssignedAssetNonChronoTags();
	
	
	
}
