package com.entappia.ei4oassetprofile.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.entappia.ei4oassetprofile.dbmodels.FirmwareSettings;

@Repository
public interface FirmwareSettingsRepository extends CrudRepository<FirmwareSettings, String> {
	FirmwareSettings findByApplication(String appName);
	
	List<FirmwareSettings> findAll();
	
	@Modifying
	@Transactional
	@Query(value = "truncate table firmware_settings", nativeQuery = true)
	void truncateMyTable();

}
