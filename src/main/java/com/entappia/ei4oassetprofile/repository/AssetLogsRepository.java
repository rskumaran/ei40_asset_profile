package com.entappia.ei4oassetprofile.repository;

import java.time.LocalDate;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.entappia.ei4oassetprofile.dbmodels.AssetLogs;
 

@Repository
public interface AssetLogsRepository extends CrudRepository<AssetLogs, Integer> {

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "DELETE FROM asset_logs WHERE date < :date", nativeQuery = true)
	int deleteByDate(@Param("date") LocalDate date);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "select * FROM asset_logs WHERE date < :date", nativeQuery = true)
	List<AssetLogs> selectLogsBeforeDate(@Param("date") LocalDate date);
}
