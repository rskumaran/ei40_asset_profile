package com.entappia.ei4oassetprofile.datacollection;

import java.util.Date;

import com.entappia.ei4oassetprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4oassetprofile.constants.AppConstants.AssignmentType;;

public class Notification {
	
	String assetType;//Chrono or non chrono
	String assetId;//The Id assigned for the asset
	AssignmentType assignmentType;//Department or to employee
	String assignedId;//The Id for the department of employee tagid
	Date expiredTime;//The to time which the asset has ended
	Date lastNotifiedTime;
	String Notification;// this string is to map the condition with the udp message type will be needed later
	NotificationType errorType;
	String NotificationStatus;// this is to display in the mail or message as status
	
	public String getNotificationStatus() {
		return NotificationStatus;
	}
	public void setNotificationStatus(String notificationStatus) {
		NotificationStatus = notificationStatus;
	}
	int notificationCount = 0;
	
	public String getAssetType() {
		return assetType;
	}
	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
	public AssignmentType getAssignmentType() {
		return assignmentType;
	}
	public void setAssignmentType(AssignmentType assignedType) {
		this.assignmentType = assignedType;
	}
	public Date getExpiredTime() {
		return expiredTime;
	}
	public void setExpiredTime(Date expiredTime) {
		this.expiredTime = expiredTime;
	}
	Date lastOccuredTime;
	boolean sendNotification;
	
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getAssignedId() {
		return assignedId;
	}
	public void setAssignedId(String assignedId) {
		this.assignedId = assignedId;
	}
	public Date getLastNotifiedTime() {
		return lastNotifiedTime;
	}
	public void setLastNotifiedTime(Date lastNotifiedTime) {
		this.lastNotifiedTime = lastNotifiedTime;
	}
	public String getNotification() {
		return Notification;
	}
	public void setNotification(String notification) {
		Notification = notification;
	}
	public NotificationType getErrorType() {
		return errorType;
	}
	public void setErrorType(NotificationType errorType) {
		this.errorType = errorType;
	}
	public int getNotificationCount() {
		return notificationCount;
	}
	public void setNotificationCount(int notificationCount) {
		this.notificationCount = notificationCount;
	}
	public Date getLastOccuredTime() {
		return lastOccuredTime;
	}
	public void setLastOccuredTime(Date lastOccuredTime) {
		this.lastOccuredTime = lastOccuredTime;
	}
	public boolean isSendNotification() {
		return sendNotification;
	}
	public void setSendNotification(boolean sendNotification) {
		this.sendNotification = sendNotification;
	}
	

}
