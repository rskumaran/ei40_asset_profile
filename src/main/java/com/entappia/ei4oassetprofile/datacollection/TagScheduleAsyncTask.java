package com.entappia.ei4oassetprofile.datacollection;

import java.sql.Time;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.entappia.ei4oassetprofile.constants.AppConstants.AssignmentType;
import com.entappia.ei4oassetprofile.constants.AppConstants.LogEventType;
import com.entappia.ei4oassetprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4oassetprofile.constants.AppConstants.ZoneType;
import com.entappia.ei4oassetprofile.dbmodels.AssetChrono;
import com.entappia.ei4oassetprofile.dbmodels.AssetChronoAllocation;
import com.entappia.ei4oassetprofile.dbmodels.AssetLogs;
import com.entappia.ei4oassetprofile.dbmodels.AssetNonChrono;
import com.entappia.ei4oassetprofile.dbmodels.AssetNonChronoAllocation;
import com.entappia.ei4oassetprofile.dbmodels.CampusDetails;
import com.entappia.ei4oassetprofile.dbmodels.Shift;
import com.entappia.ei4oassetprofile.dbmodels.Tags;
import com.entappia.ei4oassetprofile.dbmodels.ZoneDetails;
import com.entappia.ei4oassetprofile.models.AssetAssignmentDetails;
import com.entappia.ei4oassetprofile.models.NotificationEvents;
import com.entappia.ei4oassetprofile.quuppa.QuuppaApiService;
import com.entappia.ei4oassetprofile.repository.AssetChronoAllocationRepository;
import com.entappia.ei4oassetprofile.repository.AssetChronoRepository;
import com.entappia.ei4oassetprofile.repository.AssetLogsRepository;
import com.entappia.ei4oassetprofile.repository.AssetNonChronoAllocationRepository;
import com.entappia.ei4oassetprofile.repository.AssetNonChronoRepository;
import com.entappia.ei4oassetprofile.repository.CampusDetailsRepository;
import com.entappia.ei4oassetprofile.repository.ShiftRepository;
import com.entappia.ei4oassetprofile.repository.TableUtils;
import com.entappia.ei4oassetprofile.repository.TagsRepository;
import com.entappia.ei4oassetprofile.repository.ZoneRepository;
import com.entappia.ei4oassetprofile.repository.ProfileConfigurationRepository;
import com.entappia.ei4oassetprofile.udp.client.UdpClient;
import com.entappia.ei4oassetprofile.udp.client.UdpIntegrationClient;
import com.entappia.ei4oassetprofile.utils.LogEvents;
import com.entappia.ei4oassetprofile.utils.Preference;
import com.google.gson.Gson;
import com.entappia.ei4oassetprofile.utils.Utils;
import com.entappia.ei4oassetprofile.dbmodels.ProfileConfiguration;
import com.entappia.ei4oassetprofile.models.ConfigData;
import com.entappia.ei4oassetprofile.constants.AppConstants;
import com.snatik.polygon.Point;
import com.snatik.polygon.Polygon;



@Component
public class TagScheduleAsyncTask {

	public String TAG_NAME = "TagScheduleTask";

	Preference preference = new Preference();

	@Autowired
	private QuuppaApiService quuppaApiService;

	@Autowired
	private TagsRepository tagsRepository;

	@Autowired
	private CampusDetailsRepository campusDetailsRepository;

	@Autowired
	private ZoneRepository zoneRepository;

	@Autowired
	private AssetLogsRepository assetLogsRepository;

	@Autowired
	private LogEvents logEvents;


	@Autowired
	private ShiftRepository shiftRepository;

	@Autowired
	private TableUtils tableUtils;

	@Autowired
	private AssetNonChronoRepository assetnonchronoRepository;

	@Autowired
	private AssetChronoRepository assetchronoRepository;

	@Autowired
	private AssetNonChronoAllocationRepository assetNonChronoAllocationRepository;

	@Autowired
	private AssetChronoAllocationRepository assetChronoAllocationRepository;


	@Autowired
	private ProfileConfigurationRepository profileConfigurationRepository;


	private final UdpClient udpClient;

	@Autowired
	private NotificationManager notificationManager ;


	//move these functions to a separate class
	//
	private HashMap<String, List<String>> gridZoneMap;
	private JSONArray tagsJsonArray = null;
	private CampusDetails campusDetails = null;
	private JSONArray smoothedPosition = null;

	int maxDaysToKeepOldTables = AppConstants.maxDaysToKeepOldTables;
	int threadTimeDuration = AppConstants.threadTimeDuration;
	int maxDaysToKeepOldLogData = AppConstants.maxDaysToKeepOldLogData;

	LocalDate tablesDeletedDate = null;
	boolean deleteOldTables = true;
	LocalDate logsDeletedDate = null;
	boolean deleteOldLogs = true;

	private ScheduledExecutorService scheduler;
	private ScheduledFuture<?> futureTask = null;
	private Runnable myTask;

	int count = 0;

	@Autowired
	public TagScheduleAsyncTask(UdpIntegrationClient udpClient) {
		this.udpClient = udpClient;

	}

	@PostConstruct
	private void intializeThread()
	{
		scheduler =
				Executors.newScheduledThreadPool(2);

		myTask = new Runnable()
		{
			@Override
			public void run()
			{
				getTagPositions();
			}
		};
		if(threadTimeDuration > 0)
		{       
			if (futureTask != null)
			{
				futureTask.cancel(true);
			}

			futureTask = scheduler.scheduleAtFixedRate(myTask, 0, threadTimeDuration * 60, TimeUnit.SECONDS);
		}

	}
	void getTagPositionsAsync() {
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {
			public void run() {
				getTagPositions();
			}
		});
		executorService.shutdown();
	}
	int getReadInterval()
	{
		//read here from repository for changes in thread timing and return
		//need to check if 0 the thread has to be stopped or not
		ProfileConfiguration profileConfiguration = profileConfigurationRepository.selectTopRecord();
		int threadTimeDurationLocal = -1;
		if(null != profileConfiguration) {

			List<ConfigData> configDataList = profileConfiguration.getConfigData();
			if (configDataList != null && configDataList.size() > 0) {
				for (int i = 0; i < configDataList.size(); i++) {

					ConfigData configData = configDataList.get(i);

					if (configData != null) {
						 if (configData.getName().equals(AppConstants.CONFIG_AST)) {
							threadTimeDurationLocal = Integer.parseInt(configData.getDefaultval());
							if(threadTimeDurationLocal <= 0)
							{
								threadTimeDurationLocal = -1;
							}
							Utils.printInConsole("ThreadTimeDuration : "+ threadTimeDurationLocal);
						}
					}
				}
			}
		}

	
		return threadTimeDurationLocal;
	}
	public void checkAndChangeReadInterval()
	{
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		executorService.execute(new Runnable() {
			public void run() {
				int currentReadInterval = getReadInterval();

				Utils.printInConsole("current thread Interval" + currentReadInterval, true);
				if(currentReadInterval > 0) {
					Utils.printInConsole("Inside if(-1 != currentReadInterval)");
					if(currentReadInterval != threadTimeDuration) {
						threadTimeDuration = currentReadInterval;
						changeReadInterval(threadTimeDuration * 60);
					}
				}
			}
		});
		executorService.shutdown();
	}
	public void changeReadInterval(long time)
	{
		Utils.printInConsole("Inside changeReadInterval");
		if(time > 0)
		{       
			if (futureTask != null)
			{
				futureTask.cancel(true);
			}

			futureTask = scheduler.scheduleAtFixedRate(myTask, time, time, TimeUnit.SECONDS);
		}
	}

	Gson gson = new Gson();
	DecimalFormat df = new DecimalFormat("#.##");

	private boolean getTagListFromDevice()
	{
		try {
			tagsJsonArray = null;
			CompletableFuture<JSONObject> getTagCompletableFuture = quuppaApiService.getTagDetails();
			CompletableFuture.allOf(getTagCompletableFuture).join();

			if (getTagCompletableFuture.isDone()) {
				preference.setLongValue("lastscan", System.currentTimeMillis());

				JSONObject jsonObject = getTagCompletableFuture.get();
				if (jsonObject != null) {

					String status = jsonObject.optString("status");

					if (!Utils.isEmptyString(status)) {
						if (status.equals("success")) {

							JSONObject jsonTagObject = jsonObject.optJSONObject("response");
							if (jsonTagObject != null) {
								int code = jsonTagObject.getInt("code");
								if (code == 0) {
									tagsJsonArray = jsonTagObject.getJSONArray("tags");
								}
							}
						}
						else if (status.equals("error")) {
							String message = jsonObject.getString("message");
							logEvents.addLogs(LogEventType.ERROR, "GetQuuppaTag", "Get QuuppaTag-Schedule", message);

						}
					}
				}
			}
		}
		catch(Exception e) {
			logEvents.addLogs(LogEventType.ERROR, "GetQuuppaTag", "Get QuuppaTag-Schedule", e.getMessage());
			e.printStackTrace();
		}
		if(tagsJsonArray!= null)
			return true;
		return false;
	}
	private boolean getCampusDetails() {
		campusDetails = null;
		campusDetails = getCampusDetails(1);
		if(campusDetails != null) {
			return true;
		}
		return false;
	}
	private boolean searchTag(String macId) {
		smoothedPosition = null;
		for(int i=0; i<tagsJsonArray.length(); i++){
			JSONObject tagJsonObject = tagsJsonArray.getJSONObject(i);
			String id = tagJsonObject.optString("tagId", "");

			if(macId.equals(id))
			{
				smoothedPosition = tagJsonObject
						.optJSONArray("location");
				if (smoothedPosition != null)
					return true;
				else
					return false;
			}
		}

		return false;
	}
	private double getSmoothedPositionX() {
		if (smoothedPosition != null)
			return smoothedPosition.getDouble(0);
		return -1;
	}
	private double getSmoothedPositionY() {
		if (smoothedPosition != null)
			return smoothedPosition.getDouble(1);
		return -1;
	}
	private boolean getGridZoneMap() {

		gridZoneMap = new HashMap<>();
		if (campusDetails != null)
			gridZoneMap = campusDetails.getGridZoneDetails();
		else
			return false;
		if(gridZoneMap!= null) {
			return true;
		}
		return false;
	}
	private double getOriginX() {
		if(campusDetails!= null)
			return campusDetails.getOriginX()
					* campusDetails.getMetersPerPixelX();
		return -1;
	}
	private double getOriginY() {
		if(campusDetails!= null)
			return campusDetails.getOriginY()
					* campusDetails.getMetersPerPixelY();
		return -1;
	}
	//@Scheduled(cron = "0/10 * * * * *")
	//@Scheduled(cron = "0 0/1 * * * *")


	public void getTagPositions() {

		Calendar currentCalendar = Calendar.getInstance();
		currentCalendar.set(Calendar.MILLISECOND, 0);
		currentCalendar.set(Calendar.SECOND, 0);
		Date currentDate = currentCalendar.getTime();

		Utils.printInConsole("Asset Run Time:-" + Utils.getFormatedDate2(new Date()), true);



		double originX = getOriginX();
		double originY = getOriginY();

		try {

			//Get current shift name
			//If there are no active shifts the shift name 
			//will be empty. quit the process or loop when no shift is 
			//found make this function call to the start of the 
			//thread and get out complete thread execution
			Utils.printInConsole("GetCurrentShiftname");
			String currentShiftName = getCurrentShiftName(currentDate);
			if(currentShiftName == "") {
				Utils.printInConsole("No shift available for current time");
				logEvents.addLogs(LogEventType.EVENT, "Run", "Run-AssetprofileApplication",
						"No shift available for current time");
				currentShiftName = "NoShift";
				//comment this return when data need to be collected during no shift time also
				return;
			}

			//fetch all tags allocated or mapped to asset from server/DB
			//fetch from QPE one time for all the iterations
			//fetch from qpe
			Utils.printInConsole("GetTaglistfromdevice");
			if(!getTagListFromDevice()) {
				Utils.printInConsole("taglist from device not found");
				return;
			}

			Utils.printInConsole("GetCampusdetails");
			if(!getCampusDetails()) {
				Utils.printInConsole("Campusdetails Not Found");
				logEvents.addLogs(LogEventType.ERROR, "Run", "Run-AssetprofileApplication",
						"Campus Details not found");
				return;
			}
			Utils.printInConsole("GetGridzonemap");
			if(!getGridZoneMap()){
				Utils.printInConsole("Gridzone map Not Found");
				logEvents.addLogs(LogEventType.ERROR, "Run", "Run-AssetprofileApplication",
						"Grid zone map not found");
				return;
			}
			Utils.printInConsole("GetOriginX");
			originX = getOriginX();
			if(originX == -1) {
				logEvents.addLogs(LogEventType.ERROR, "Run", "Run-AssetprofileApplication",
						"originX not Found ");
				return;
			}
			Utils.printInConsole("GetOriginY");
			originY = getOriginY();
			if(originY == -1) {
				logEvents.addLogs(LogEventType.ERROR, "Run", "Run-AssetprofileApplication",
						"originX not Found");
				return;
			}

			ArrayList<Tags> tagList = new ArrayList<Tags>();
			ArrayList<Tags> chronoTagList = new ArrayList<Tags>();
			ArrayList<Tags> nonChronoTagList = new ArrayList<Tags>();

			chronoTagList = (ArrayList<Tags>) tagsRepository.selectAssignedAssetChronoTags();
			nonChronoTagList = (ArrayList<Tags>) tagsRepository.selectAssignedAssetNonChronoTags();


			tagList.addAll(chronoTagList);
			tagList.addAll(nonChronoTagList);


			for (int i = 0;i < tagList.size();i++){

				Tags assetTag = tagList.get(i);


				//append date and shift name as per table strategy
				String currentTableName = getCurrentTableName(currentShiftName, assetTag.getType());
				if(currentTableName == "") {
					Utils.printInConsole("Table Name Formatting error");
					logEvents.addLogs(LogEventType.ERROR, "Run", "Run-AssetprofileApplication",
							"Table Name formatting error");
					return;
				}
				Utils.printInConsole(currentTableName);
				//Check whether table exists
				//Create new table if not exists
				tableUtils.createLocationTable(currentTableName);

				double x = -1;
				double y = -1;


				if(searchTag(assetTag.getMacId())) {

					x = getSmoothedPositionX();
					y = getSmoothedPositionY();

					x = Math.abs(originX - x);
					y = Math.abs(originY - y);

					//notify

				}
				else { 
					//tag not found
					logEvents.addLogs(LogEventType.ERROR, "Run", "Run-assetprofileApplication",
							"Tag : " + assetTag.getTagId() + " assigned to an Asset :" + assetTag.getAssignmentId() + " is missing" );
					Utils.printInConsole("tag Not Found");
					//notify
					//return;
				}
				Utils.printInConsole("GetZoneDetails");
				//check tag exists
				ZoneDetails zoneDetails = getZoneDetails(gridZoneMap, x, y);
				Utils.printInConsole("GetZoneDetails x: " + x + " y: " + y);
				if (zoneDetails != null){
					ZoneType currentZoneType = zoneDetails.getZoneType();
					String zoneName = zoneDetails.getName();

					if (zoneDetails != null && currentZoneType != ZoneType.DeadZone) {

						/*Utils.printInConsole("Time in Minutes : " + Utils.getDayMintus(currentDate));
								Utils.printInConsole("Asset ID : " + assetTag.getAssignmentId());
								Utils.printInConsole("Zone ID : " + zoneDetails.getId());
								Utils.printInConsole("Zone Name : " + zoneName);
								Utils.printInConsole("Grid xPosition : " + getGridPosition(x));
								Utils.printInConsole("Grid yposition : " + getGridPosition(y));
								Utils.printInConsole("Grid Number : " + ((getGridPosition(x)-1) * Math.ceil(campusDetails.getWidth()) + getGridPosition(y)));*/

						int GridNumber = (int) ((getGridPosition(x)-1) * Math.ceil(campusDetails.getWidth()) + getGridPosition(y));

						int xValue = getGridPosition(x);
						int yValue = getGridPosition(y);

						String gridPosition = xValue + "_" + yValue;

						String assetTagAssignmentID = assetTag.getAssignmentId();
						int zoneId = zoneDetails.getId();
						int currentTimeinMinutes = Utils.getDayMintus(currentDate);

						tableUtils.insertIntoLocationTable(currentTableName, assetTagAssignmentID, currentTimeinMinutes,
								GridNumber, zoneId,zoneName, gridPosition, threadTimeDuration);

					} else {
						//Tag in Dead zone 
						Utils.printInConsole("Asset is in Non Tracking Zone (Dead zone)");
						//return;
						logEvents.addLogs(LogEventType.EVENT, "Run", "Run-AssetprofileApplication",
								"Asset is in Non Tracking Zone (Dead zone)" );

					}

				}

				else
				{
					Utils.printInConsole("zone details not found");
					logEvents.addLogs(LogEventType.ERROR, "Run", "Run-AssetprofileApplication",
							"ZoneDetails x: " + x + " y: " + y + "is not found" );

				}
			}
			// write here for asset late
			ArrayList<AssetNonChronoAllocation> assetNonChronoAssignedList = new ArrayList<AssetNonChronoAllocation>();
			ArrayList<AssetChronoAllocation> assetChronoAssignedList = new ArrayList<AssetChronoAllocation>();

			assetNonChronoAssignedList = assetNonChronoAllocationRepository.getAllValidAssets();
			assetChronoAssignedList = assetChronoAllocationRepository.getAllValidAssets();

			//SimpleDateFormat assetDateFormat = new SimpleDateFormat("yyyy-MMM-dd");
			SimpleDateFormat assetTimeFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm");


			for(int i=0; i < assetNonChronoAssignedList.size();i++)
			{
				AssetNonChronoAllocation assetNonChronoAllocation = assetNonChronoAssignedList.get(i);

				AssetNonChrono assetNonChrono = assetnonchronoRepository.findByAssetId(assetNonChronoAllocation.getAssetId());

				HashMap<String, AssetAssignmentDetails> assignmentdetailsMap= assetNonChrono.getAssetAssignmentDetails();

				if(assignmentdetailsMap != null)
				{
					AssetAssignmentDetails assignmentdetails = assignmentdetailsMap.get(assetNonChronoAllocation.getMacId());

					if(assignmentdetails != null)
					{


						Date currentTime = new Date();
						Date assetFromTime = assetTimeFormat.parse(assignmentdetails.getFromTime());
						Date assetToTime = assetTimeFormat.parse(assignmentdetails.getToTime());
						//Date assetDate = assetDateFormat.parse(assignmentdetails.getDate());
						//if given asset time is crossed 
						// then send notification

						Utils.printInConsole("CurrentTime : " + currentTime + " assetFromTime" + assetFromTime + " assetToTime" + assetToTime);

						if (checkAllowedTime(currentTime, assetFromTime, assetToTime) == false){
							//notify

							/*Calendar calendar = Calendar.getInstance();
									calendar.clear();
									calendar.setTime(assetToTime);
									calendar.set(assetDate.getYear() + 1900,assetDate.getMonth() , assetDate.getDate());
									assetToTime = calendar.getTime();*/

							/*addNotification(String assetType, String assetId, AssignmentType assignmentType, String assignedId, Date expiredTime,
											String sNotification, NotificationType errorType)*/

							notificationManager.addNotification("NonChrono",assetNonChronoAllocation.getAssetId(), assetNonChronoAllocation.getAssignmentType(),
									assetNonChronoAllocation.getMacId(), assetToTime, "lateAsset", NotificationType.EMERGENCY, "Late Asset"); //zoneName );

							Utils.printInConsole("asset ID : " + assetNonChronoAllocation.getAssetId() +" macID : "+ assetNonChronoAllocation.getMacId() +"time limit crossed");
							//continue;
						}
						else{
							//notify
							notificationManager.removeNotification("NonChrono",assetNonChronoAllocation.getAssetId(), assetNonChronoAllocation.getAssignmentType(),
									assetNonChronoAllocation.getMacId(), "lateAsset");
						}
					}
				}

			}
			for(int i=0; i < assetChronoAssignedList.size();i++)
			{
				AssetChronoAllocation assetChronoAllocation = assetChronoAssignedList.get(i);

				AssetChrono assetChrono = assetchronoRepository.findByAssetId(assetChronoAllocation.getAssetId());

				HashMap<String, AssetAssignmentDetails> assignmentdetailsMap= assetChrono.getAssetAssignmentDetails();

				if(assignmentdetailsMap != null)
				{
					AssetAssignmentDetails assignmentdetails = assignmentdetailsMap.get(assetChronoAllocation.getMacId());

					if(assignmentdetails != null)
					{

						Date currentTime = new Date();
						Date assetFromTime = assetTimeFormat.parse(assignmentdetails.getFromTime());
						Date assetToTime = assetTimeFormat.parse(assignmentdetails.getToTime());
						//Date assetDate = assetDateFormat.parse(assignmentdetails.getDate());
						//if given asset time is crossed
						// then send notification

						if (checkAllowedTime(currentTime, assetFromTime, assetToTime) == false){

							/*Calendar calendar = Calendar.getInstance();
									calendar.clear();
									calendar.setTime(assetToTime);
									calendar.set(assetDate.getYear() + 1900,assetDate.getMonth() , assetDate.getDate());
									assetToTime = calendar.getTime();*/

							/*addNotification(String assetType, String assetId, AssignmentType assignmentType, String assignedId, Date expiredTime,
											String sNotification, NotificationType errorType)*/

							//notify
							notificationManager.addNotification("Chrono",assetChronoAllocation.getAssetId(), assetChronoAllocation.getAssignmentType(),
									assetChronoAllocation.getMacId(), assetToTime, "lateAsset", NotificationType.EMERGENCY, "Late Asset"); //zoneName );


							Utils.printInConsole("asset ID : " + assetChronoAllocation.getAssetId() +" macID : "+ assetChronoAllocation.getMacId() +"time limit crossed");
							//continue;
						}
						else{
							//notify
							notificationManager.removeNotification("Chrono",assetChronoAllocation.getAssetId(), assetChronoAllocation.getAssignmentType(),
									assetChronoAllocation.getMacId(), "lateAsset");

						}
					}
				}
			}






		}
		catch(Exception e) {
			e.printStackTrace(); 
		}

		notificationManager.sendNotification();
		checkAndChangeReadInterval();
		checkAndModifyConfigurationData();
		modifyTableData();
		/*JSONObject jsonObject = new JSONObject();
				jsonObject.put("status", 200);
				jsonObject.put("message", "Tag data read done");
				udpClient.sendMessage(jsonObject.toString());*/
	}


	private ZoneDetails getZoneDetails(HashMap<String, List<String>> gridZoneMap, double x, double y) {

		int xValue = getGridPosition(x);
		int yValue = getGridPosition(y);

		if (gridZoneMap.containsKey(xValue + "_" + yValue)) {

			List<String> zoneIdList = gridZoneMap.get(xValue + "_" + yValue);
			if (zoneIdList != null && zoneIdList.size() > 0) {
				if (zoneIdList.size() == 1) {
					String zoneId = zoneIdList.get(0);
					Optional<ZoneDetails> zoneDetails = zoneRepository.findById(Integer.valueOf(zoneId));
					if (zoneDetails != null && zoneDetails.isPresent() && zoneDetails.get().isActive())
						return zoneDetails.get();
				} else {
					for (String zoneId : zoneIdList) {
						Optional<ZoneDetails> zoneDetails = zoneRepository.findById(Integer.valueOf(zoneId));
						if (zoneDetails != null && zoneDetails.isPresent() && zoneDetails.get().isActive()) {

							ZoneDetails zoneDetails1 = zoneDetails.get();
							List<HashMap<String, String>> coordinateList = zoneDetails1.getCoordinateList();
							if (isInsidePolygon(coordinateList, x, y))
								return zoneDetails1;
						}
					}
				}
			}
		}

		return null;
	}

	private boolean isInsidePolygon(List<HashMap<String, String>> coordinateList, double x, double y) {
		Polygon.Builder polygonBuilder = Polygon.Builder();

		for (int j = 0; j < coordinateList.size(); j++) {
			HashMap<String, String> coordinate = coordinateList.get(j);

			double x1 = Double.valueOf(coordinate.get("x"));
			double y1 = Double.valueOf(coordinate.get("y"));

			polygonBuilder.addVertex(new Point(x1, y1));

		}

		polygonBuilder.close();

		Polygon polygon = polygonBuilder.build();
		Point point = new Point(x, y);
		boolean contains = polygon.contains(point);
		if (contains) {
			return true;
		} else
			return false;
	}

	private int getGridPosition(double num1) {
		int value = 0;
		if (num1 % 1 == 0)
			value = (int) Math.floor(num1);
		else
			value = (int) Math.floor(num1 + 1);

		return value - 1;
	}

	public CampusDetails getCampusDetails(int campusId) {
		return campusDetailsRepository.findByCampusId(campusId);
	}

	public HashMap<String, List<String>> getCampusGridList() {
		//gridZoneMap = new HashMap<>();
		//campusDetails = campusDetailsRepository.findByCampusId(1);
		if (campusDetails != null)
			gridZoneMap = campusDetails.getGridZoneDetails();

		return gridZoneMap;
	}

	protected boolean checkAllowedTime(Date currentTime, Date assetFromTime, Date assetToTime) {

		if(currentTime.after(assetFromTime) && currentTime.before(assetToTime)){
			return true;
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	protected boolean checkAllowedTimeNew(Date currentTime, Date assetFromTime, Date assetToTime, Date assetDate) {

		Utils.printInConsole("Inside checkAllowedTime");
		Utils.printInConsole("CurrentTime : " + currentTime + " assetFromTime" + assetFromTime + " assetToTime" + assetToTime);

		Date assetFromTime1,assetToTime1;

		Calendar calendar = Calendar.getInstance();
		TimeZone tz = Calendar.getInstance().getTimeZone();

		calendar.clear();
		calendar.setTimeZone(tz) ;
		if (tz.useDaylightTime()) {
			assetFromTime.setHours(assetFromTime.getHours()-(tz.getDSTSavings()/3600000));
			assetToTime.setHours(assetToTime.getHours()-(tz.getDSTSavings()/3600000));
		}


		calendar.setTime(assetFromTime);
		calendar.set(assetDate.getYear() + 1900,assetDate.getMonth() , assetDate.getDate());
		assetFromTime1 = calendar.getTime();

		calendar.clear();
		calendar.setTimeZone(tz) ;
		calendar.setTime(assetToTime);
		calendar.set(assetDate.getYear() + 1900,assetDate.getMonth() , assetDate.getDate());
		assetToTime1 = calendar.getTime();

		Utils.printInConsole("After converting");
		Utils.printInConsole("CurrentTime : " + currentTime + " assetFromTime" + assetFromTime + " assetToTime" + assetToTime);

		if(currentTime.after(assetFromTime1) && currentTime.before(assetToTime1)){
			return true;
		}
		return false;
	}
	// GetCurrentTableName

	// converts string according to the table name rules

	// need to handle this issue if no shift is found

	private String getCurrentTableName(String shiftName, String tagtypeList) {

		String currentTableName = "";

		String profileName = tagtypeList + "_" + "Location";

		Date currentDate = new Date();

		// Table name hours and minutes can be used to find out in case of issues

		// For only date is used we can change it later

		// SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy HH:mm:ss");


		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");

		String scurrentDate = formatter.format(currentDate);

		currentTableName = profileName + "_" + scurrentDate + "_" + shiftName;

		return currentTableName;

	}

	//GetCurrentShiftName
	//The current working shift is needed to append in the table name 
	//to find the current working shift the current time is checked 
	//whether it lies between shift start time shift end time 
	// the issue occurs when two shifts are running in the same time
	// have to handle it 
	private String getCurrentShiftName(Date currentTime)
	{
		//Get current shift name from the shift start time and 
		ArrayList<Shift> shiftDetailsArr = (ArrayList<Shift>) shiftRepository.findActiveShifts();


		String shiftName = "";
		for (int i = 0;i < shiftDetailsArr.size();i++){
			Shift shiftElement = shiftDetailsArr.get(i);
			Date shiftStartTime = shiftElement.getShiftStartTime();
			Date shiftEndTime = shiftElement.getShiftEndTime();

			Utils.printInConsole("shiftStartTime" + shiftStartTime);
			Utils.printInConsole("shiftEndTime" + shiftEndTime);

			Calendar calendar = Calendar.getInstance();
			calendar.clear();
			calendar.setTime(shiftStartTime);
			calendar.set(currentTime.getYear() + 1900,currentTime.getMonth() , currentTime.getDate());
			shiftStartTime = calendar.getTime();

			calendar.clear();
			calendar.setTime(shiftEndTime);
			calendar.set(currentTime.getYear() + 1900,currentTime.getMonth() , currentTime.getDate());
			shiftEndTime = calendar.getTime();

			if(currentTime.after(shiftStartTime) && currentTime.before(shiftEndTime)){
				shiftName = shiftElement.getShiftName(); break; }
			if(currentTime.equals(shiftStartTime)){
				shiftName = shiftElement.getShiftName();
				break;
			}

		}

		return shiftName;
	}

	void checkAndModifyConfigurationData() {
		try {
			ProfileConfiguration profileConfiguration = profileConfigurationRepository.selectTopRecord();
			if(null != profileConfiguration) {

				List<ConfigData> configDataList = profileConfiguration.getConfigData();
				if (configDataList != null && configDataList.size() > 0) {
					for (int i = 0; i < configDataList.size(); i++) {

						ConfigData configData = configDataList.get(i);

						if (configData != null) {

							if (configData.getName().equals(AppConstants.CONFIG_TTA))
							{
								maxDaysToKeepOldTables = Integer.parseInt(configData.getDefaultval());
								if(maxDaysToKeepOldTables >= AppConstants.minDaysToKeepOldTables && maxDaysToKeepOldTables <= AppConstants.maxDaysToKeepOldTables)
								{
									if(maxDaysToKeepOldTables != AppConstants.maxDaysToKeepOldTables) {
										deleteOldTables = true;
									}

									if(null == tablesDeletedDate)
									{
										deleteOldTables = true;
									}
									else {//database deleted date not NULL
										LocalDate currentDate = LocalDate.now();
										if(!tablesDeletedDate.isEqual(currentDate)) {
											deleteOldTables = true;
										}
									}

								}
								else
								{
									maxDaysToKeepOldTables = AppConstants.maxDaysToKeepOldTables;
									deleteOldTables = true;
								}

							}
							else if (configData.getName().equals(AppConstants.CONFIG_LTA))
							{
								maxDaysToKeepOldLogData = Integer.parseInt(configData.getDefaultval());

								if(maxDaysToKeepOldTables >= AppConstants.minDaysToKeepOldTables && maxDaysToKeepOldTables <= AppConstants.maxDaysToKeepOldTables)
								{
									if(maxDaysToKeepOldLogData != AppConstants.maxDaysToKeepOldLogData) {
										deleteOldLogs = true;
									}

									if(null == logsDeletedDate)
									{
										deleteOldLogs = true;
									}
									else {//database deleted date not NULL
										LocalDate currentDate = LocalDate.now();
										if(!logsDeletedDate.isEqual(currentDate)) {
											deleteOldLogs = true;
										}
									}
								}
								else
								{
									maxDaysToKeepOldLogData = AppConstants.maxDaysToKeepOldLogData;
									deleteOldLogs = true;
								}

							}
							else if (configData.getName().equals(AppConstants.CONFIG_ARI)) 
							{
								int intervalBetweenNotifications = Integer.parseInt(configData.getDefaultval());
								if(intervalBetweenNotifications > 0)
								{
									notificationManager.setIntervalBetweenNotifications(intervalBetweenNotifications);
								}
								Utils.printInConsole("intervalBetweenNotifications : "+ intervalBetweenNotifications);
							}
							else if (configData.getName().equals(AppConstants.CONFIG_ARC)) {
								int maxNotificationCount = Integer.parseInt(configData.getDefaultval());
								if(maxNotificationCount >= 0)
								{
									notificationManager.setMaxNotificationCount(maxNotificationCount);
								}
								Utils.printInConsole("maxNotificationCount : "+ maxNotificationCount);
							}

						}

					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

	void modifyTableData(){

		if(deleteOldTables) {
			LocalDate lastDateToKeepLocationTable = getLastDateToKeepLocationTable(maxDaysToKeepOldTables);
			deleteOldLocationTable(lastDateToKeepLocationTable);
			//ToDo check for if there are error conditons and set flag to false
			Utils.printInConsole("maxDaysToKeepOldTables : " + maxDaysToKeepOldTables + "Date : "+ lastDateToKeepLocationTable + "deleteOldTables : "+ deleteOldTables);
			deleteOldTables = false;
		}
		if(deleteOldLogs) {
			LocalDate lastDateToKeepLogData = getLastDateToKeepLogData(maxDaysToKeepOldLogData);
			deleteOldLogs(lastDateToKeepLogData);
			//ToDo check for if there are error conditons and set flag to false
			Utils.printInConsole("maxDaysToKeepOldLogData : " + maxDaysToKeepOldLogData + "Date : "+ lastDateToKeepLogData + "deleteOldTables : "+ deleteOldLogs);
			deleteOldLogs = false;
		}

	}

	private void deleteOldLogs(LocalDate lastDateToKeepLogData) {

		int queryResult = assetLogsRepository.deleteByDate(lastDateToKeepLogData );
		Utils.printInConsole("Delete Query Result : " + queryResult);


		List<AssetLogs> Logs = (List<AssetLogs>)assetLogsRepository.selectLogsBeforeDate(lastDateToKeepLogData );
		if(Logs != null)
		{
			if(Logs.size() == 0) {
				Utils.printInConsole("No Records found");
			}
			for(int j=0;j<Logs.size();j++)
			{
				Utils.printInConsole("Date : "+ Logs.get(j).getDate() + "Result : "+ Logs.get(j).getResult());
			}
		}
		else {
			Utils.printInConsole("Logs null");
		}
	}

	private LocalDate getLastDateToKeepLogData(int daysToKeepOldLogData2) {
		LocalDate LastDate = LocalDate.now().minusDays(maxDaysToKeepOldTables);
		return LastDate;
	}

	@SuppressWarnings("unchecked")
	private void deleteOldLocationTable(LocalDate lastDateToKeepLocationTable) {

		DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy/MM/dd");
		String lastDate = lastDateToKeepLocationTable.format(formatters);
		List<String> tableNames = (List<String>)tableUtils.SelectTableNamesLike(getAssetChronoTableName(), lastDate );
		if(tableNames != null)
		{
			for(int j=0;j<tableNames.size();j++)
			{
				Utils.printInConsole("In Between chrono Table Names : " + tableNames.get(j) + "last date : "+ lastDateToKeepLocationTable.toString());
				tableUtils.dropLocationTable(tableNames.get(j));
			}
		}
		tableNames = (List<String>)tableUtils.SelectTableNamesLike(getAssetNonChronoTableName(), lastDate );
		if(tableNames != null)
		{
			for(int j=0;j<tableNames.size();j++)
			{
				Utils.printInConsole("In Between non chrono Table Names : " + tableNames.get(j) + "last date : "+ lastDateToKeepLocationTable.toString());
				tableUtils.dropLocationTable(tableNames.get(j));
			}
		}
	}

	private LocalDate getLastDateToKeepLocationTable(int maxDaysToKeepOldTables) {

		LocalDate LastDate = LocalDate.now().minusDays(maxDaysToKeepOldTables);

		return LastDate;
	}

	private String getAssetChronoTableName() {
		return getTableName("Asset Chrono");

	}
	private String getAssetNonChronoTableName() {
		return getTableName("Asset NonChrono");
	}

	private String getTableName(String tableName) {
		String currentTableName = "";
		String profileName = tableName + "_Location";

		currentTableName = profileName + "_";

		return currentTableName;
	}



}
