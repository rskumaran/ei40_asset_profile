package com.entappia.ei4oassetprofile.datacollection;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.entappia.ei4oassetprofile.constants.AppConstants;
import com.entappia.ei4oassetprofile.constants.AppConstants.AssignmentType;
import com.entappia.ei4oassetprofile.constants.AppConstants.NotificationType;
import com.entappia.ei4oassetprofile.udp.client.UdpClient;
import com.entappia.ei4oassetprofile.udp.client.UdpIntegrationClient;
import com.entappia.ei4oassetprofile.utils.Utils;
import com.entappia.ei4oassetprofile.dbmodels.Notifications;
import com.entappia.ei4oassetprofile.models.NotificationDetailMessages;
import com.entappia.ei4oassetprofile.repository.NotificationsRepository;

@Component
public class NotificationManager {
	
	private UdpClient mUdpClient = null;
	@Autowired
	private NotificationsRepository notificationRepository;
	
	ArrayList<Notification> notificationList = new ArrayList<>();
	
	int intervalBetweenNotifications = AppConstants.intervalbetweenNotifications;//Default in minutes
	int maxNotificationCount = AppConstants.maxNotificationCount;// Default
	int minutesToKeepPreviousNotification = AppConstants.minutesToKeepPreviousNotification;//Default
	
	/*@Autowired
	public NotificationManager() {
	}*/
	@Autowired
	public NotificationManager(UdpIntegrationClient udpClient) {
		this.mUdpClient = udpClient;
		Utils.printInConsole("Inside Notification Manager Constructor");
	}
	
	public void setIntervalBetweenNotifications(int intervalBetweenNotifications) {
		this.intervalBetweenNotifications = intervalBetweenNotifications;
	}


	public void setMaxNotificationCount(int maxNotificationCount) {
		this.maxNotificationCount = maxNotificationCount;
	}

	public void addNotification(String assetType, String assetId, AssignmentType assignmentType, String assignedId, Date expiredTime,
			String sNotification, NotificationType errorType, String sNotificationStatus)
	{
		boolean notificationFoundInList =  false;
		for(int i = 0; i < notificationList.size();i++)
		{
			Notification notification = notificationList.get(i);
		
			if(notification.getAssetType().equals(assetType) && notification.getAssignedId().equals(assignedId) && notification.getAssetId().equals(assetId) 
					&& notification.getNotification().equals(sNotification)) {
				notificationFoundInList = true;
				notification.setErrorType(errorType);
				notification.setLastOccuredTime(new Date());
				notification.setSendNotification(true);
				notification.setExpiredTime(expiredTime);
			}
			
		}
		if(notificationFoundInList == false) {
			Notification notification = new Notification();
			notification.setAssetType(assetType);
			notification.setAssetId(assetId);
			notification.setAssignmentType(assignmentType);
			notification.setAssignedId(assignedId);
			notification.setExpiredTime(expiredTime);
			notification.setNotificationStatus(sNotificationStatus);
			notification.setNotification(sNotification);
			
			notification.setErrorType(errorType);
			notification.setLastOccuredTime(new Date());
			notification.setSendNotification(true);
			notificationList.add(notification);
		}
		
	}
	public void removeNotification(String assetType, String assetId, AssignmentType assignmentType, String assignedId,String sNotificationStatus)
	{
		for(int i = notificationList.size() - 1; i >= 0;i--)
		{
			Notification notification = notificationList.get(i);
		
			if(notification.getAssetType().equals(assetType) && notification.getAssignedId().equals(assignedId) && notification.getAssetId().equals(assetId) 
					&& notification.getNotificationStatus().equals(sNotificationStatus))  {
				
				notificationList.remove(i);
			}
			
		}
	}
	public void sendNotification()
	{

		//List<HashMap<String, String>> lnotificationTagsList = new ArrayList<>();

		HashMap<String, List<HashMap<String, String>>> lnotificationTagsList = new HashMap<>();

		NotificationType notificationType = NotificationType.INFORMATION;

		for(int i = 0; i < notificationList.size();i++)
		{
			Notification notification = notificationList.get(i);

			if(notification.isSendNotification() == true) {

				//check here for time expire and count for sending notification

				if(canNotify(notification) ==  true ) {
					
					notification.setSendNotification(true);
					notification.setLastNotifiedTime(new Date());
					notification.setNotificationCount(notification.getNotificationCount() + 1);
					
					if(notification.getNotification().equalsIgnoreCase("lateAsset")){

						HashMap<String, String> notificationmap = new HashMap<>();

						notificationmap.put("id", notification.getAssetId());
						//notificationmap.put("assettype", notification.getAssetType());
						notificationmap.put("uid", notification.getAssignedId());
						notificationmap.put("type", notification.getAssignmentType().toString());
						notificationmap.put("notificationCount", String.valueOf(notification.getNotificationCount()));
						
						Date endDate = notification.getExpiredTime();
						SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
						String sEndDate = formatter.format(endDate);
						notificationmap.put("time", sEndDate );

						if(lnotificationTagsList.containsKey(notification.getAssetType()))
						{
							List<HashMap<String, String>> mapList = lnotificationTagsList.get(notification.getAssetType());

							if(mapList==null)
								mapList = new ArrayList<>();

							mapList.add(notificationmap);
							lnotificationTagsList.put(notification.getAssetType(), mapList); 

						}else
						{
							List<HashMap<String, String>> mapList= new ArrayList<>();
							mapList.add(notificationmap);
							lnotificationTagsList.put(notification.getAssetType(), mapList); 

						}

					}

					

					if( notification.getErrorType().compareTo(notificationType) > 0) {
						notificationType = notification.getErrorType();
					}
				}


				//The last Notified Time and the Notification count is updated only during the 
				//time of sending notifications

			}
			else
			{
				notification.setSendNotification(false);
			}

		}

		if (lnotificationTagsList.size() > 0) {

			JSONObject mJSONArray = new JSONObject(lnotificationTagsList);

			JSONObject jsonObject = new JSONObject();
			jsonObject.put("status", 200);
			jsonObject.put("type", "lateAsset");
			jsonObject.put("data", mJSONArray);
			mUdpClient.sendMessage(jsonObject.toString());

			NotificationDetailMessages notificationDetailMessages = new NotificationDetailMessages();
			notificationDetailMessages.setType("lateAsset");
			notificationDetailMessages.setTagData(lnotificationTagsList);

			Notifications notifications = new Notifications();
			notifications.setType(notificationType);
			notifications.setSource("asset_profile");

			
			//Here frame message for chrono and non chrono separately check inner map also 
			Integer sum = lnotificationTagsList.values().stream().mapToInt(List::size).sum();
			//change to sum and check inner values upto three
			//if(lnotificationTagsList.size()>1)
			if(sum > 1)
			{
				notifications.setMessage(sum + " assets allocated time period exceeded");
				notifications.setMessageObj(notificationDetailMessages);
			}
			else{  


				Map.Entry<String, List<HashMap<String, String>>> entry = lnotificationTagsList.entrySet().iterator().next();

				List<HashMap<String, String>> value = entry.getValue();
				if(value.size()>1) {
					notifications.setMessage(value.size() + " assets allocated time period exceeded");
					notifications.setMessageObj(notificationDetailMessages);
				}else
				{
					notifications.setMessage( "asset " + value.get(0).get("uid") + " allocated time period exceeded");
					notifications.setMessageObj(null);
				}
			}

			notifications.setNotifyTime(new Date());
			notificationRepository.save(notifications);
		}
		
		removeExpiredNotifications();

	}

	private void removeExpiredNotifications() {
		for(int i = notificationList.size() - 1; i >= 0;i--)
		{
			Notification notification = notificationList.get(i);
			Date lastNotifiedTime = notification.getLastNotifiedTime();
			Date currentTime = new Date();
			long duration = currentTime.getTime() - lastNotifiedTime.getTime();

			long diffMinutes = duration / (60 * 1000) % 60; 
			
			if(diffMinutes > minutesToKeepPreviousNotification) {
				
				notificationList.remove(i);
			}
			
		}
	}

	private boolean canNotify(Notification notification) {
	
		//Check all the possible conditions to send notification and returns
		//whether particular notification can be sent or not
		if(notification.getLastNotifiedTime() != null)
		{
		Date lastNotified = notification.getLastNotifiedTime();
		Date currentDate = new Date();
		long diff = currentDate.getTime() - lastNotified.getTime();
		
		//long diffSeconds = diff / 1000 % 60;  
		long diffMinutes = diff / (60 * 1000) % 60; 
		//long diffHours = diff / (60 * 60 * 1000);
		
		if(notification.getNotificationCount() < maxNotificationCount &&  diffMinutes >= intervalBetweenNotifications)
		{
			return true;
		}
		}
		else 
		{
			return true;
		}
		return false;
	}

	public void setUdpClient(UdpIntegrationClient udpClient) {
		
		this.mUdpClient = udpClient;
	}
	
	

}
